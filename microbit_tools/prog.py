import os
import time
#import dbus
import subprocess
import re

#from bluezero import async_tools

#from bluezero import adapter
#from bluezero import microbit

from openhtf import plugs
from shutil import copy
from spintop_openhtf.plugs.base import UnboundPlug

devices_regex = re.compile(r'([\S ]*)\(([0-9A-Z:]+)\)')

HERE = os.path.abspath(os.path.dirname(__file__))


class MicrobitProgrammingPlug(UnboundPlug):
    def __init__(self, disk):
        super(MicrobitProgrammingPlug, self).__init__()
        #self.dongle = adapter.Adapter()
        #self.dongle.on_device_found = self._device_found
        self._device = None
        self.disk = disk

    def program_microbit(self, binary):
        copy(binary,self.disk)


    # def remove_microbits_devices(self):
        # for device_alias, addr in self.list_devices():
            # if 'micro:bit' in device_alias:
                # subprocess.check_output(['bt-device', '-r', addr])

    # def list_devices(self):
        # output = subprocess.Popen(['bt-device', '-l'], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read().decode()
        # return [(m.group(1), m.group(2)) for m in devices_regex.finditer(output)]

    # def _device_found(self, device_obj):
        # if 'micro:bit' in device_obj.alias:
            # try:
                # self._device = self.create_microbit(device_obj)
            # finally:
                # self.tearDown()

    # def create_microbit(self, device_obj):
        # ubit = microbit.Microbit(adapter_addr=self.dongle.address,
                                        # device_addr=device_obj.address,
                                        # accelerometer_service=True,
                                        # button_service=True,
                                        # led_service=True,
                                        # magnetometer_service=False,
                                        # pin_service=False,
                                        # temperature_service=True)
        # # ubit.connect()
        # return ubit

    # def tearDown(self):
        # try:
            # self.dongle.stop_discovery()
        # except dbus.exceptions.DBusException:
            # pass
        # finally:
            # self.dongle.mainloop.quit()
            # self.eloop.quit()

    # def discover(self, timeout=10):
        
        # self.eloop = async_tools.EventLoop()

        # def start_disco():
            # self.dongle.start_discovery()

        # def exit_disco():
            # self.tearDown()

        # self.remove_microbits_devices()
        
        # self.eloop.add_timer(500, start_disco)
        # self.eloop.add_timer(timeout*1000, exit_disco)

        # self.eloop.run()

        # if self._device is None:
            # self.tearDown()
            # raise Exception('Timeout while waiting for discovery')
        
        # return self._device
            
