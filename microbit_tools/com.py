
from spintop_openhtf.plugs.iointerface.comport import ComportInterface


class MicrobitComPlug(ComportInterface):

	def __init__(self, comport, baudrate=115200):
		super().__init__(comport, baudrate)
	
	def open_with_log(self):
		self.log_to_filename('com.txt')
		self.open()

	def verify_boot(self, target):
		target = "micro:bit ready" 
		log = self.com_target("",target, timeout=10, keeplines=0)	
		
	def wait_prompt(self):
		log = self.com_target("",">>>", timeout=5, keeplines=0)	

	def read_accelerometer(self): 

		self.clear_lines()
		self.write("accelerometer.get_x()\r\n")
		log = self.com_target("",">>>", timeout=10, keeplines=0)	
		lines = log.splitlines()
		x = int(lines[1])
		
		self.clear_lines()
		self.write("accelerometer.get_y()\r\n")
		log = self.com_target("",">>>", timeout=10, keeplines=0)	
		lines = log.splitlines()
		y = int(lines[1])
		
		self.clear_lines()
		self.write("accelerometer.get_z()\r\n")
		log = self.com_target("",">>>", timeout=10, keeplines=0)	
		lines = log.splitlines()
		z = int(lines[1])
		
		return [x,y,z]

	def read_magnetometer(self):
		
		#log = com.com_target("compass.get_field_strength()\r\n",">>>", timeout=1, keeplines=0)
		self.clear_lines()
		self.write("compass.get_field_strength()\r\n")
		line = self.next_line()
		line = self.next_line()
		
		value = int(line)
		return value

	def verify_button(self, button):
		log = self.com_target("","Button {}".format(button), timeout=15, keeplines=2)

	