# main.py
import os

import openhtf as htf
from openhtf import PhaseResult
from openhtf.plugs.user_input import UserInput
from openhtf.util import conf

from spintop_openhtf import TestPlan
from test_case_library import PowerSupplies 
from test_case_library import Programming 
from test_case_library import Devices
from test_case_library import EdgeConnector
from criteria.criteria import get_criteria

from microbit_tools.prog import MicrobitProgrammingPlug
from microbit_tools.com import MicrobitComPlug


from sequences.prog import create_programming_sequence
from sequences.voltage import create_voltage_sequence
from sequences.sensors import create_sensors_sequence

from pprint import pprint

""" Test Plan """
# This defines the name of the testbench.
plan = TestPlan('microbit')


#import test station configuration
conf.declare("comport")
conf.declare("baudrate")
conf.declare("disk")
conf.load_from_filename(r"test_station_config\config.yml")


#define plugs
mbcom = MicrobitComPlug.as_plug( 'mbcom', comport=conf.comport,  baudrate=conf.baudrate)
mbprog = MicrobitProgrammingPlug.as_plug( 'mbprog', disk=conf.disk)

@plan.testcase('TSC000_Setup')
@plan.plug(com=mbcom)
def setup(test, com):
	""" Executes the preparatory steps for the proper operation of the testbench
		 - It opens the microbit COM Port	"""
	try:
		com.open_with_log()
		test.logger.info ("COM Port open")
	except:
		test.logger.info ("COM Port open failed")
		return PhaseResult.STOP
		
plan.append(create_voltage_sequence())
plan.append(create_programming_sequence(conf, mbprog, mbcom))
plan.append(create_sensors_sequence(mbprog, mbcom))

if __name__ == '__main__':
    conf.load(station_server_port='4444', capture_docstring=True)
    plan.run()
	