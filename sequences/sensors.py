import openhtf as htf
from spintop_openhtf import TestPlan, TestSequence
from openhtf.plugs.user_input import UserInput
from openhtf import PhaseResult

from spintop_openhtf import TestPlan
from test_case_library import PowerSupplies 
from test_case_library import Programming 
from test_case_library import Devices
from test_case_library import EdgeConnector
from criteria.criteria import get_criteria

from microbit_tools.prog import MicrobitProgrammingPlug
from microbit_tools.com import MicrobitComPlug



def create_sensors_sequence(mbprog, mbcom):


    sequence = TestSequence('Voltage Sequence')

    @sequence.testcase('TSC020_Magnetometer_Test')
    @sequence.plug(com=mbcom)
    @htf.measures(get_criteria("magnetometer"))
    def execute_mag(test, com):
        """Verifies that the magnetometer is functionnal and can be accessed by the MCU"""
        Devices.MagnetometerTest(test, com)
        
    @sequence.testcase('TSC021_Accelerometer_Test')
    @sequence.plug(com=mbcom)
    @htf.measures(get_criteria("accel_x"))
    @htf.measures(get_criteria("accel_y"))
    @htf.measures(get_criteria("accel_z"))
    def execute_acc(test, com):
        """Verifies that the accelerometer is functional and can be accessed by the MCU"""
        Devices.AccelerometerTest(test, com)
        
    @sequence.testcase('TSC022_LED_Test')
    @htf.plugs.plug(prompts=UserInput)
    @sequence.plug(prog=mbprog)
    @sequence.plug(com=mbcom)
    def execute_led(test,prompts,prog,com):
        """Verifies each LED (LED1 through 25) is functional (can be individually turned ON and OFF)"""
        Devices.LEDTest(test, prompts,prog,com)
        
    @sequence.testcase('TSC023_PushButton_Test')
    @htf.plugs.plug(prompts=UserInput)
    @sequence.plug(prog=mbprog)
    @sequence.plug(com=mbcom)
    def execute_button(test, prompts, prog, com):
        """Verifies each push button (A & B) is functional (each position can be resolved individually)"""
        Devices.PushButtonTest(test, prompts, prog, com)
        
    @sequence.testcase('TSC040_Edge_Connector_ADC_Test')
    def execute_adc(test):
        EdgeConnector.ADCTest(test)
        
        
    

    return sequence