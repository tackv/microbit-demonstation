import openhtf as htf
from spintop_openhtf import TestPlan, TestSequence
from openhtf.plugs.user_input import UserInput
from openhtf import PhaseResult

from spintop_openhtf import TestPlan
from test_case_library import PowerSupplies 
from test_case_library import Programming 
from test_case_library import Devices
from test_case_library import EdgeConnector
from criteria.criteria import get_criteria

from microbit_tools.prog import MicrobitProgrammingPlug
from microbit_tools.com import MicrobitComPlug



def create_voltage_sequence():


    sequence = TestSequence('Voltage Sequence')

    @sequence.testcase('TSC001_Impedances_Test')
    @htf.plugs.plug(prompts=UserInput)
    @htf.measures(get_criteria("impedance"))
    def execute_impedance(test, prompts):
        """Verifies no shorts are present on the board power supplies and that the unit can be powered safely"""
        PowerSupplies.ImpedanceTest(test, prompts)
        
    @sequence.testcase('TSC002_Voltage_Test')
    @htf.plugs.plug(prompts=UserInput)
    @htf.measures(get_criteria("main_voltage"))
    def execute_voltage(test, prompts):
        """ Verifies that the voltage supplied by the USB connector is within expected range"""
        PowerSupplies.MainVoltageTest(test, prompts)
        
    @sequence.testcase('TSC003_Battery_Test')
    def execute_battery(test):
        PowerSupplies.BatteryTest(test)
        
    

    return sequence



 