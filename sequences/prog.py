import openhtf as htf
from spintop_openhtf import TestPlan, TestSequence
from openhtf.plugs.user_input import UserInput
from openhtf import PhaseResult

from spintop_openhtf import TestPlan
from test_case_library import PowerSupplies 
from test_case_library import Programming 
from test_case_library import Devices
from test_case_library import EdgeConnector
from criteria.criteria import get_criteria


def create_programming_sequence(conf, mbcom, mbprog):

    sequence = TestSequence('Voltage Sequence')

    @sequence.testcase('TSC010_Programming_Test')
    @sequence.plug(prog=mbprog)
    @sequence.plug(com=mbcom)
    def execute_programming(test, prog, com):
        """ Programs the unit, verifies the unit boots and tests the serial port"""
        Programming.ProgrammingTest(test, prog, com)
        
    @sequence.testcase('TSC011_Reset_Test')
    @htf.plugs.plug(prompts=UserInput)
    @sequence.plug(com=mbcom)
    def execute_reset(test, prompts, com):
        """ Verifies that the reset button is functional"""
        Programming.ResetTest(test, prompts, com)
        
    

    return sequence



 