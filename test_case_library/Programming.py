from static_config.programming import ProgConf as prog
from time import sleep
from openhtf import PhaseResult


def ProgrammingTest(test, microbit, com):
	try:
		test.logger.info ("Programming Test")
		microbit.program_microbit(prog.GENERIC_BIN)
		res = com.verify_boot(">>>")
		if res == False:
			test.logger.info ("Programming Failed")
			return PhaseResult.STOP
		else:
			test.logger.info ("Programming Success")
	except:
		return PhaseResult.STOP

def ResetTest(test, prompts, com):
	try:
		test.logger.info ("Reset Test")
		prompts.prompt('To perform the reset test, hit OKAY below and press the reset button')
		res = com.verify_boot(">>>")
			
	except:
		test.logger.info ("Reset Failed")
		return PhaseResult.FAIL_AND_CONTINUE
