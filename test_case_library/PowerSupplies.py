
from openhtf import PhaseResult

def ImpedanceTest(test, prompts):
	FORM_LAYOUT = {
		'schema':{
			'title': "Impedance",
			'type': "object",
			'required': ["impedance"],
			'properties': {
				'impedance': {
					'type': "string", 
					'title': "Measure Impedance on test point X\nEnter value in Ohms"
				},
			}
		},
		'layout':[
			"impedance"
		]
	}
	
	test.logger.info ("Impedance Test")
	response = prompts.prompt_form(FORM_LAYOUT)
	test.logger.info ("Impedance measured is {}".format(response["impedance"]))
	
	test.measurements.impedance = int(response["impedance"])



def MainVoltageTest(test, prompts):
	FORM_LAYOUT = {
		'schema':{
			'title': "Main Voltage",
			'type': "object",
			'required': ["main_voltage"],
			'properties': {
				'main_voltage': {
					'type': "string", 
					'title': "Measure Voltage on test point X\nEnter value in Volts"
				},
			}
		},
		'layout':[
			"main_voltage"
		]
	}
		
	test.logger.info ("Main Voltage Test")	
	response = prompts.prompt_form(FORM_LAYOUT)
	test.logger.info ("Voltage measured is {}".format(response["main_voltage"]))
	
	test.measurements.main_voltage = float(response["main_voltage"])
	

def BatteryTest(test ):

	test.logger.info ("Battery Test")
	