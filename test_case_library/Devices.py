from openhtf.plugs.user_input import UserInput, PromptType
from openhtf import PhaseResult

from static_config.programming import ProgConf as prog

from time import sleep

def MagnetometerTest(test, com):

	try:
		test.logger.info ("Magnetometer Test")
		res = com.read_magnetometer()
		test.logger.info ("Magnetometer measurement is {}".format(res))
	
	except Exception as e:
		test.logger.info ("Magnetometer measurement failed")
		test.logger.info (e)
		return PhaseResult.FAIL_AND_CONTINUE
	
	test.measurements.magnetometer = res
	
def AccelerometerTest(test, com):

	try:
		test.logger.info ("Accelerometer Test")
		res = com.read_accelerometer()
		test.logger.info ("Accelerometer measurement is {} {} {}".format(res[0],res[1],res[2]))

	except Exception as e:
		test.logger.info ("Accelerometer measurement failed")
		test.logger.info (e)
		return PhaseResult.FAIL_AND_CONTINUE
		

	test.measurements.accel_x = int(res[0])
	test.measurements.accel_y = int(res[1])
	test.measurements.accel_z = int(res[2])
	test.logger.info ("Accelerometer test passed")
		

def LEDTest(test, prompts, microbit, com):

	try:
		test.logger.info ("LED Test")
		prompts.prompt('A LED walk will be executed on the micro:bit. Press OKAY to launch the programming of the device and the LED walk. Verify that all LEDs are functional')
		microbit.program_microbit(prog.LED_BIN)
		com.verify_boot("micro:bit ready - led walk")
		test.logger.info ("LED Walk programming success")
		
	except: 
		test.logger.info ("LED Walk programming failed")
		return PhaseResult.FAIL_AND_CONTINUE
		
	try:
		result = prompts.prompt('Press PASS if all LEDs are functional. Press FAIL if an error is detected', prompt_type=PromptType.PASS_FAIL)
		test.logger.info ("LED Walk validation without error")
		
	except:
		test.logger.info ("LED Walk validation found an error")
		return PhaseResult.FAIL_AND_CONTINUE
		
	
	
def PushButtonTest(test, prompts, microbit,com):

	button_error = False
	try:
		test.logger.info ("PushButton Test")
		microbit.program_microbit(prog.BUTTON_BIN)
		com.verify_boot("micro:bit ready - button")
		test.logger.info ("PushButton Test programming success")
		
	except:
		test.logger.info ("PushButton Test programming failed")
		return PhaseResult.FAIL_AND_CONTINUE
		
	try:
		prompts.prompt('Press button A')
		com.verify_button("A")
		test.logger.info ("Button A read successfully")
		
	except:
		test.logger.info ("Button A read failed")
		button_error = True
		
		
	try:
		prompts.prompt('Press button B')
		com.verify_button("B")
		test.logger.info ("Button B read successfully")
		
	except:
		test.logger.info ("Button B read failed")
		button_error = True
	
	if button_error == True	:
		test.logger.info ("PushButton Test Failed")
		return PhaseResult.FAIL_AND_CONTINUE
		
	