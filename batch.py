# main.py
import os

import openhtf as htf
from openhtf.plugs.user_input import UserInput
from openhtf.util import conf

from spintop_openhtf import TestPlan
from test_case_library import GaussianTest
from specific_test_function_library import data_generation



""" Test Plan """

# This defines the name of the testbench.
plan = TestPlan('examples.hello_world')


@plan.testcase('Gaussian_Test')
def execute_gaussian_test(test):
    GaussianTest.GaussianTest()

if __name__ == '__main__':
    conf.load(station_server_port='4444', capture_docstring=True)
    plan.run()
	