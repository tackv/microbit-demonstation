import openhtf as htf

def get_criteria(criteria):
	crit_dict = {
			"impedance":    htf.Measurement('impedance').    in_range(25, 100),
			"main_voltage": htf.Measurement('main_voltage'). in_range(3.1, 3.5),
			"magnetometer": htf.Measurement('magnetometer'). in_range(50000, 100000),
			"accel_x":      htf.Measurement('accel_x').      in_range(-150, 150),
			"accel_y":      htf.Measurement('accel_y').      in_range(-150, 150),
			"accel_z":      htf.Measurement('accel_z').      in_range(-1500, -800)
			}
	return crit_dict[criteria]